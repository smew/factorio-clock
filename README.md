# factorio-clock

![](screenshot.png)

Program that sends a command to a running Factorio server instance to spawn a clock with the current time. All connected players see the same time.

Set it as a cron job to run every minute.

## Requirements

In order to send commands to the Factorio instance, factorio-init needs to be installed.

https://github.com/Bisa/factorio-init 

## Setting concrete

When running for the first time, you can set a concrete background by giving the additional argument `floor`. This only needs to be done once, as concrete tiles are not removed at each update.
