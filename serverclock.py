import time
import sys
import subprocess

digits = {
    "0" : 
    [
        1,1,1,
        1,0,1,
        1,0,1,
        1,0,1,
        1,1,1
    ],
    "1" : 
    [
        0,0,1,
        0,0,1,
        0,0,1,
        0,0,1,
        0,0,1
    ],
    "2" : 
    [
        1,1,1,
        0,0,1,
        1,1,1,
        1,0,0,
        1,1,1
    ],
    "3" : 
    [
        1,1,1,
        0,0,1,
        1,1,1,
        0,0,1,
        1,1,1
    ],
    "4" : 
    [
        1,0,1,
        1,0,1,
        1,1,1,
        0,0,1,
        0,0,1
    ],
    "5" : 
    [
        1,1,1,
        1,0,0,
        1,1,1,
        0,0,1,
        1,1,1
    ],
    "6" : 
    [
        1,1,1,
        1,0,0,
        1,1,1,
        1,0,1,
        1,1,1
    ],
    "7" : 
    [
        1,1,1,
        0,0,1,
        0,0,1,
        0,0,1,
        0,0,1
    ],
    "8" : 
    [
        1,1,1,
        1,0,1,
        1,1,1,
        1,0,1,
        1,1,1
    ],
    "9" : 
    [
        1,1,1,
        1,0,1,
        1,1,1,
        0,0,1,
        0,0,1
    ],
    ":" : 
    [
        0,0,0,
        0,1,0,
        0,0,0,
        0,1,0,
        0,0,0
    ]
}

char_width  = 3
char_height = 5

border_top    = -9
border_left   = -10

border_width  = 21
border_height = 7

def set_wall(x, y):
    command = '/silent-command game.surfaces[1].create_entity{{name="stone-wall", position={{ {}, {} }} }}'.format(x, y)
    send_command(command)

def set_concrete(top, left, bottom, right):
    patches = ""
    for x in range(left, right):
        for y in range(top, bottom):
            patches += '{{name="concrete", position={{ {}, {} }}}},'.format(x, y)
    command = '/silent-command game.surfaces[1].set_tiles{{ {} }}'.format(patches)
    send_command(command)

def clear_walls(top, left, bottom, right):
    command = '/silent-command for _, entity in pairs(game.surfaces[1].find_entities_filtered{{area = {{ {{ {}, {} }}, {{ {}, {} }} }}, type = "wall"}}) do entity.destroy() end'.format(left, top, right, bottom)
    send_command(command)

def set_digit(digit, position):
    for i, val in enumerate(digit):
        if val == 1:
            x = border_left + 1
            y = border_top + 1
            x += i % 3 + position * (char_width + 1)
            y += i // 3
            set_wall(x, y)

def set_time():
    time_string = time.strftime('%H:%M')
    for i in range(len(time_string)):
        set_digit(digits[time_string[i]], i)

def send_command(command):
    subprocess.call(["/opt/factorio-init/factorio", "cmd", command])

if __name__== "__main__":
    if(len(sys.argv) > 1 and sys.argv[1] == "floor"):
        set_concrete(border_top, border_left, border_top + border_height, border_left + border_width)

    clear_walls(border_top, border_left, border_top + border_height, border_left + border_width)
    set_time()

